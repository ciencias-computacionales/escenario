﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentScript : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;

    private GameObject objective;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        GameObject[] objectives = GameObject.FindGameObjectsWithTag("Objective");

        int size = objectives.Length;
        int index = Random.Range(0, size);

        Debug.Log(index);
        objective = objectives[index];
    }

    void FixedUpdate ()
    {

        Vector3 centerPoint = objective.transform.position;

        Vector3 movement = centerPoint - rb.transform.position;
        movement.Normalize();

        rb.AddForce (movement * speed);
    }
}
