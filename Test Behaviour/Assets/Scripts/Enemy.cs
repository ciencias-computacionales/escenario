﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float visionRadius;
    public float speed;

    private Rigidbody rb;

    GameObject Player;

    Vector3 initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        Player = GameObject.FindGameObjectWithTag("Mono");
        initialPosition = transform.position;
    
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = initialPosition;

        float dist = Vector3.Distance(Player.transform.position, transform.position);
        if (dist < visionRadius) target = Player.transform.position;

        float fixedspeed = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, fixedspeed);

        Debug.DrawLine(transform.position, target, Color.green);

        //rb.AddForce(/*Debug.DrawLine*/ transform.position * speed);

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);

    }

}
