﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class cameraFollowing : MonoBehaviour
{
    //public float speed;

    //private Rigidbody rb;

    public GameObject Player;

    public Vector3 PosicionRelativa;

    void Start()
    {
        /*rb = GetComponent<Rigidbody>();
        GameObject[] objectives = GameObject.FindGameObjectsWithTag("Objective");

        int size = objectives.Length;
        int index = Random.Range(0, size);

        Debug.Log(index);
        objective = objectives[index];*/

        PosicionRelativa = transform.position - Player.transform.position;
    }

    /*void FixedUpdate()
    {

        Vector3 centerPoint = objective.transform.position;

        Vector3 movement = centerPoint - rb.transform.position;
        movement.Normalize();

        rb.AddForce(movement * speed);*/
    public void LateUpdate()
    {
        transform.position = Player.transform.position + PosicionRelativa;
    }
}

