﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;

public class moveXZ: MonoBehaviour
{
    public float velocidad;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void /*Fixed*/Update()
    {
        float tecladoHorizontal = Input.GetAxisRaw("Horizontal");
        float posX = transform.position.x + (tecladoHorizontal * velocidad * Time.deltaTime);

        float tecladoVertical = Input.GetAxisRaw("Vertical");
        float posZ = transform.position.z + (tecladoVertical * velocidad * Time.deltaTime);

        transform.position = new Vector3(posX, transform.position.y, posZ);

        //Vector3 movement = new Vector3(tecladoHorizontal, 0.0f, tecladoVertical);

        rb.AddForce(transform.position * velocidad);
    }
}
