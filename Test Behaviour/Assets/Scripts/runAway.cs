﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class runAway : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;

    public GameObject objective;

    //public Vector3 PosicionRelativa;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GameObject[] objectives = GameObject.FindGameObjectsWithTag("Agent");

        int size = objectives.Length;
        int index = Random.Range(0, size);

        Debug.Log(index);
        objective = objectives[index];

        //PosicionRelativa = transform.position - MH1.transform.position;
    }

    void FixedUpdate()
    {

        Vector3 centerPoint = -objective.transform.position;

        Vector3 movement = centerPoint + rb.transform.position;
        movement.Normalize();

        rb.AddForce(movement * speed);
        
    /*public void LateUpdate()
    {
        
        transform.position = MH1.transform.position + PosicionRelativa;*/
        
    }
}
